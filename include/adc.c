#inclue "adc.h"

void adc_init()
{
	ADMUX = (1<<REFS0);
	ADCSRA = (1<<ADEN)|(1<<ADPS2)|(0<<ADPS1)|(1<<ADPS0);
}

int adc_read(char ch)
{
	ch &= 0b00000111; 
	ADMUX = (ADMUX & 0xF8)|ch; 
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));

	return (ADC);
}