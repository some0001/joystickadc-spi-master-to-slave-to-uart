#include <avr/io.h>
#define BAUDRATE 103
#define BAUD 9600

void UART_init();
void UART_send(unsigned char ch);
void UART_sendString(char str[], short len);
unsigned char UART_receive(void);
void UART_receiveString(char str[], short len);
void send(unsigned char ch);
void terminate();
