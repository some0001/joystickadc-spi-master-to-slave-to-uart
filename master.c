#define F_CPU 16000000UL

#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <util/delay.h>
#include "include/spiPB.h"
#include "include/adc.h"

#define highByte(x) (x >> 8)
#define lowByte(x) (x & 0xff)

volatile int Xpos = 0;    // Global variable holding X position read from ADC0
volatile int Ypos = 0;    // Global variable holding X position read from ADC1
void timer1_init(); //Compare timer that triggers interrupt which sends XY coordinates to SPI slaves
void buttonInterrupt_init(short port); //External port interrupt for button at DDRB
volatile char button_pushed = 0;
volatile char update = 0;

int main(void)
{
	DDRB |= (1<<0);
	// Init sequence
	buttonInterrupt_init(1);
	timer1_init();
	spi_initMasterPB();
	adc_init();
	sei();
		
	while (1)
	{
		Xpos = adc_read(0);
		Ypos = adc_read(1);
		
		if(button_pushed == 1 && (PINB & (1<<1)))
		{
			spi_sendPB('B');
			_delay_ms(12);
			spi_sendPB(0xFF);
			if(spi_transmit(0) == 'L')
			{
				PORTB ^= (1<<0);
			}

			_delay_ms(5);
			button_pushed = 0;
		}	
		
		if(update == 1)	
		{
			spi_sendPB('C');
			spi_sendPB( highByte(Xpos) );
			spi_sendPB( lowByte(Xpos) );	
					
			spi_sendPB( highByte(Ypos) );
			spi_sendPB( lowByte(Ypos) );
			update = 0;
		}
	}
}


void buttonInterrupt_init(short port)
{
	DDRB &= ~(1 << port);
	PORTB |= (1 << port);
	PCICR |= (1 << PCIE0);
	PCMSK0 |= (1 << PCINT1);
}



void timer1_init(void)
{
	TCCR1B |= (1 << WGM12); 
	TCCR1B |= (1 << CS12); 
	TCNT1 = 0; 
	OCR1A = 0xffff-1;

	TIMSK1 |= (1 << OCIE1A);
}


ISR(PCINT0_vect)
{
	button_pushed = 1;
}

ISR(TIMER1_COMPA_vect)
{
	update = 1;
}
