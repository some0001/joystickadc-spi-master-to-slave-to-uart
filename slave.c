#define F_CPU 16000000UL

#include "include/spi.h"
#include "include/uart.h"
#include <stdio.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/interrupt.h>

volatile int Xpos;
volatile int Ypos;
char strX[10];
char strY[10];
volatile unsigned char h,l;


int main(void)
{
	spi_initSlave();
	UART_init();
	
	while (1)
	{
		switch(spi_receive())
		{
			case 'C':
    			h = spi_receive();
    			l = spi_receive();
    			Xpos = (int)((h << 8) | l );
    			
    			h = spi_receive();
    			l = spi_receive();
    			Ypos = (int)((h << 8) | l );
    			
    			for(int i = 0; i < 10; i++) {strX[i] = ' '; strY[i] = ' ';}
    			itoa(Xpos,strX,10); itoa(Ypos,strY,10);
    			UART_sendString("X:\0", 3);
    			UART_sendString(strX, 5);
    			UART_sendString("Y:\0", 3);
    			UART_sendString(strY, 5);
    			break;
			case 'B':
    			UART_send('B');
    			if(spi_receive() == 0xFF)
    			{
    				SPDR = UDR0;
    			}
			    break;
		}
	}
}


